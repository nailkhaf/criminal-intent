package com.example.nail.criminalintent

import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment

class CrimeListActivity : SingleFragmentActivity(), CrimeListFragment.Callback, CrimeFragment.Callback {
	override fun createFragment(): Fragment = CrimeListFragment()

	@LayoutRes
	override fun getLayoutResId(): Int {
		return R.layout.activity_masterdetail
	}

	override fun onCrimeSelected(crimeId: Long) {
		if (resources.getBoolean(R.bool.is_phone)) {
			val intent = CrimePagerActivity.newIntent(this, crimeId)
			startActivity(intent)
		} else {
			val crimeFragment = CrimeFragment.newFragment(crimeId)
			supportFragmentManager.beginTransaction()
					.replace(R.id.detail_fragment_container, crimeFragment)
					.commit()
		}
	}

	override fun onCrimeUpdated() {
		val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
		if (fragment is CrimeListFragment) {
			fragment.loadRecyclerData()
		}
	}

}
