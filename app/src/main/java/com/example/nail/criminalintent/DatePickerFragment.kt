package com.example.nail.criminalintent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_date.*
import java.util.*

class DatePickerFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_date, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val date = arguments?.get(ARG_DATE) as? Date
                ?: throw IllegalArgumentException("arguments is null")

        val cal = Calendar.getInstance()
        cal.time = date

        date_picker.init(cal[Calendar.YEAR], cal[Calendar.MONTH], cal[Calendar.DAY_OF_MONTH]) { dp, _, _, _ ->
            val newDate = GregorianCalendar(dp.year, dp.month, dp.dayOfMonth,
                    cal[Calendar.HOUR_OF_DAY], cal[Calendar.MINUTE], cal[Calendar.SECOND]).time
            sendResult(Activity.RESULT_OK, newDate)
        }
    }

    private fun sendResult(resultCode: Int, date: Date) {
        val intent = Intent().apply {
            putExtra(EXTRA_DATE, date)
        }

        targetFragment?.onActivityResult(targetRequestCode, resultCode, intent)
        activity?.setResult(resultCode, intent)
    }

    companion object {
        private const val ARG_DATE = "date"
        private const val EXTRA_DATE = "com.example.nail.criminalintent.DatePickerFragment.date"

        fun newInstance(date: Date): DatePickerFragment {
            return DatePickerFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_DATE, date)
                }
            }

        }

        fun readDate(data: Intent): Date {
            return data.getSerializableExtra(EXTRA_DATE) as Date
        }
    }
}