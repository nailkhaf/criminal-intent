package com.example.nail.criminalintent

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "crimes")
data class Crime(
		@PrimaryKey(autoGenerate = true)
		var id: Long,
		var title: String,
		var date: Long,
		var solved: Boolean,
		var requiresPolicy: Boolean,
		var suspect: String) {

	val photoName: String
		get() = "img_$id.jpg"

	@Ignore constructor() : this(
			0,
			"",
			System.currentTimeMillis(),
			false,
			false,
			"")


}