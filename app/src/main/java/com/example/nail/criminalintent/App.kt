package com.example.nail.criminalintent

import android.app.Application
import android.arch.persistence.room.Room


class App : Application() {

    companion object {
        lateinit var crimeDatabase: CrimeDatabase
    }

    override fun onCreate() {
        super.onCreate()
        crimeDatabase = Room.databaseBuilder(applicationContext,
                CrimeDatabase::class.java, CrimeDatabase.DATABASE_NAME).build()

    }
}