package com.example.nail.criminalintent

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.io.File

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
	addTextChangedListener(object : TextWatcher {

		override fun afterTextChanged(p0: Editable?) {
			afterTextChanged.invoke(p0.toString())
		}

		override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

		}

		override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
			afterTextChanged.invoke(p0.toString())
		}
	})
}

fun ViewPager.onPageSelected(onPageSelected: (Int) -> Unit) {
	addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
		override fun onPageScrollStateChanged(state: Int) {
		}

		override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
		}

		override fun onPageSelected(position: Int) {
			onPageSelected.invoke(position)
		}

	})
}

fun RecyclerView.onSwiped(onSwiped: (RecyclerView.ViewHolder, Int) -> Unit): ItemTouchHelper {
	return ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
		override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
			return false
		}

		override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
			onSwiped(viewHolder, direction)
		}
	}).apply {
		this.attachToRecyclerView(this@onSwiped)
	}
}

fun File.getScaledBitmap(path: String, destWidth: Int, destHeight: Int): Bitmap? {
	val options = BitmapFactory.Options().apply {
		inJustDecodeBounds = true
	}
	BitmapFactory.decodeFile(path, options)

	val srcWidth = options.outWidth
	val srcHeight = options.outHeight

	val newInSampleSize = if (srcWidth > destWidth || srcHeight > destHeight)
		Math.max(srcWidth / destWidth, srcHeight / destHeight) else 1

	val scaledOptions = BitmapFactory.Options().apply {
		inSampleSize = newInSampleSize
	}

	return BitmapFactory.decodeFile(path, scaledOptions)
}

fun File.getScaledBitmap(path: String, activity: Activity): Bitmap? {
	val point = Point()
	activity.windowManager.defaultDisplay.getSize(point)
	return getScaledBitmap(path, point.x, point.y)
}