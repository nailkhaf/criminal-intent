package com.example.nail.criminalintent

object CrimeLab {
    private val crimesMap = mutableMapOf<Long, Crime>()
    val size
        get() = crimesMap.size

    lateinit var crimeDao: CrimeDao

    fun getCrimes(): List<Crime> {
        return crimesMap.values.toList()
    }

    fun getCrime(id: Long): Crime {
        return crimesMap[id] ?: throw IllegalStateException()
    }

    fun saveCrime(crime: Crime): Crime {
//        crimesMap[crime.id] = crime
        return crime
    }

    fun deleteCrime(id: Long) {
        crimesMap.remove(id) ?: throw IllegalStateException()
    }
}