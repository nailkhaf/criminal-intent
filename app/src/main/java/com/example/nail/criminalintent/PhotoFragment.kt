package com.example.nail.criminalintent

import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_photo.*

class PhotoFragment : DialogFragment() {

	private val disposables = CompositeDisposable()

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.dialog_photo, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val path = arguments?.get(ARG_PATH) as? String
				?: throw IllegalArgumentException("arguments is null")

		with(dialog_photo) {
			disposables += Observable.just(path)
					.subscribeOn(Schedulers.io())
					.map { BitmapFactory.decodeFile(it) }
					.observeOn(AndroidSchedulers.mainThread())
					.doOnNext { this.setImageBitmap(it) }
					.subscribe()

			setOnClickListener {
				fragmentManager!!.beginTransaction().remove(this@PhotoFragment).commit()
			}
		}
	}

	override fun onStop() {
		super.onStop()
		disposables.clear()
	}

	companion object {
		private const val ARG_PATH = "path"

		fun newInstance(photoPath: String): PhotoFragment {
			return PhotoFragment().apply {
				arguments = Bundle().apply {
					putString(ARG_PATH, photoPath)
				}
			}
		}
	}
}