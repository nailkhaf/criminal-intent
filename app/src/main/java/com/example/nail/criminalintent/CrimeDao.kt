package com.example.nail.criminalintent

import android.arch.persistence.room.*
import io.reactivex.Single

@Dao
interface CrimeDao {

	@Insert
	fun insert(crime: Crime): Long

	@Update
	fun update(crime: Crime)

	@Delete
	fun delete(crime: Crime)

	@Query("select count(*) from crimes")
	fun size(): Long

	@Query("select * from crimes")
	fun getAll(): Single<List<Crime>>

	@Query("select * from crimes where id=:id")
	fun getOne(id: Long): Single<Crime>

}