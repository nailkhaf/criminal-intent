package com.example.nail.criminalintent

import android.support.v4.app.Fragment

class DatePickerActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return DatePickerFragment()
    }

}