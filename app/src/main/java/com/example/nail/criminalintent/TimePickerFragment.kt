package com.example.nail.criminalintent

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.TimePicker
import java.util.*

class TimePickerFragment : DialogFragment() {

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

		val date = arguments?.get(ARG_TIME) as? Date
				?: throw IllegalArgumentException("arguments is null")

		val cal = Calendar.getInstance()
		cal.time = date


		return AlertDialog.Builder(activity!!).apply {
			val view = LayoutInflater.from(activity!!).inflate(R.layout.dialog_time, null)
			val timePicker = view.findViewById<TimePicker>(R.id.dialog_time_picker)

			setView(view)
			setTitle(R.string.date_picker_title)
			setPositiveButton(android.R.string.ok) { _, _ ->

				@Suppress("DEPRECATION")
				val newDate = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
					GregorianCalendar(cal[Calendar.YEAR], cal[Calendar.MONTH], cal[Calendar.DAY_OF_MONTH],
							timePicker.hour, timePicker.minute).time
				else GregorianCalendar(cal[Calendar.YEAR], cal[Calendar.MONTH], cal[Calendar.DAY_OF_MONTH],
						timePicker.currentHour, timePicker.currentMinute).time

				sendResult(Activity.RESULT_OK, newDate)
			}
			with(timePicker) {
				setIs24HourView(true)
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
					hour = cal[Calendar.HOUR]
					minute = cal[Calendar.MINUTE]
				} else {
					@Suppress("DEPRECATION")
					currentHour = cal[Calendar.HOUR]
					@Suppress("DEPRECATION")
					currentMinute = cal[Calendar.MINUTE]
				}
			}

		}.create()
	}

	private fun sendResult(resultCode: Int, date: Date) {
		targetFragment?.apply {
			val intent = Intent().apply {
				putExtra(EXTRA_DATE, date)
			}
			onActivityResult(this@TimePickerFragment.targetRequestCode, resultCode, intent)
		}
	}

	companion object {
		private const val ARG_TIME = "date"
		private const val EXTRA_DATE = "com.example.nail.criminalintent.TimePickerFragment.time"

		fun newInstance(date: Date): TimePickerFragment {
			return TimePickerFragment().apply {
				arguments = Bundle().apply {
					putSerializable(ARG_TIME, date)
				}
			}

		}

		fun readDate(data: Intent): Date {
			return data.getSerializableExtra(EXTRA_DATE) as Date
		}
	}
}