package com.example.nail.criminalintent

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_crime_pager.*

class CrimePagerActivity : AppCompatActivity() {

	private lateinit var pagerAdapter: PagerAdapter

	private lateinit var singleGetCrimes: Single<List<Crime>>

	private var crimesLastIndex = 0

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_crime_pager)

		val id = intent.getSerializableExtra(EXTRA_CRIME_ID) as Long

		pagerAdapter = PagerAdapter(supportFragmentManager)

		with(crime_view_pager) {
			adapter = pagerAdapter
			onPageSelected { nextBackButtonsVisibility(0, crimesLastIndex) }
		}

		first_item_button.setOnClickListener {
			crime_view_pager.setCurrentItem(0, true)
		}

		last_item_button.setOnClickListener {
			crime_view_pager.setCurrentItem(crimesLastIndex, true)
		}

		singleGetCrimes = App.crimeDatabase.crimeDao().getAll()
				.subscribeOn(Schedulers.io())
				.observeOn(mainThread())

		singleGetCrimes
				.doOnSuccess {
					pagerAdapter.updateCrimes(it)
					crimesLastIndex = it.lastIndex
					crime_view_pager.setCurrentItem(it.indexOfFirst { it.id == id }, true)
				}
				.doOnSuccess { nextBackButtonsVisibility(0, it.lastIndex) }
				.subscribe()
	}

	private fun nextBackButtonsVisibility(min: Int, max: Int) {
		first_item_button.visibility = if (crime_view_pager.currentItem != min) View.VISIBLE else View.GONE
		last_item_button.visibility = if (crime_view_pager.currentItem != max) View.VISIBLE else View.GONE
	}

	class PagerAdapter(fm: FragmentManager, private var crimes: List<Crime> = listOf()) : FragmentStatePagerAdapter(fm) {

		override fun getItem(position: Int) = CrimeFragment.newFragment(crimes[position].id)

		override fun getCount() = crimes.size

		fun updateCrimes(newCrimes: List<Crime>) {
			crimes = newCrimes
			notifyDataSetChanged()
		}
	}

	companion object {
		private const val EXTRA_CRIME_ID = "com.example.nail.criminalintent.CrimePagerActivity.crime_id"

		fun newIntent(context: Context, crimeId: Long): Intent {
			val intent = Intent(context, CrimePagerActivity::class.java)
			intent.putExtra(EXTRA_CRIME_ID, crimeId)
			return intent
		}
	}
}