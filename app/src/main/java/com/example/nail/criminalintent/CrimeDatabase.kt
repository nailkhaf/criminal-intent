package com.example.nail.criminalintent

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [Crime::class], version = 1, exportSchema = false)
abstract class CrimeDatabase : RoomDatabase() {

    abstract fun crimeDao(): CrimeDao

    companion object {
        const val DATABASE_NAME = "crimes.db"
    }

}
