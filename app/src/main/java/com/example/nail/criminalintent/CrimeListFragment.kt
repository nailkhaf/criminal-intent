package com.example.nail.criminalintent

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers.io
import kotlinx.android.synthetic.main.fragment_crime_list.*
import java.lang.ref.WeakReference

class CrimeListFragment : Fragment() {

	private val disposables = CompositeDisposable()

	private lateinit var crimeAdapter: CrimeAdapter
	private lateinit var crimeLayoutManager: RecyclerView.LayoutManager

	private lateinit var singleGetCrimes: Single<List<Crime>>
	private lateinit var singleSaveCrime: Single<Long>
	private lateinit var singleCountCrimes: Single<Long>

	private var callback: Callback? = null

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		if (context is Callback) {
			callback = context
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)

		crimeAdapter = CrimeAdapter(callback = callback)
		crimeLayoutManager = LinearLayoutManager(activity)

		singleGetCrimes = App.crimeDatabase.crimeDao().getAll()
				.subscribeOn(io())
				.observeOn(mainThread())

		singleSaveCrime = Single
				.fromCallable { App.crimeDatabase.crimeDao().insert(Crime()) }
				.subscribeOn(io())
				.observeOn(mainThread())

		singleCountCrimes = Single.fromCallable { App.crimeDatabase.crimeDao().size() }
				.subscribeOn(io())
				.observeOn(mainThread())

	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_crime_list, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		with(crime_recycler_view) {
			layoutManager = crimeLayoutManager
			adapter = crimeAdapter
			onSwiped { viewHolder, _ ->
				val position = viewHolder.adapterPosition
				val crime = crimeAdapter.removeCrime(position)
				disposables += Single.fromCallable { App.crimeDatabase.crimeDao().delete(crime) }
						.subscribeOn(io())
						.subscribe()
			}
		}

		loadRecyclerData()
	}

	override fun onResume() {
		super.onResume()
		loadRecyclerData()
	}

	override fun onDestroy() {
		super.onDestroy()
		disposables.clear()
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater)
		inflater.inflate(R.menu.fragment_crime_list, menu)
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
		R.id.new_crime -> {
			disposables += singleSaveCrime
					.doOnSuccess {
						callback?.onCrimeSelected(it)
						loadRecyclerData()
					}
					.subscribe()
			true
		}
		R.id.show_subtitle -> {
			disposables += singleCountCrimes
					.doOnSuccess { item.title = it.toString() }
					.subscribe()
			true
		}
		else -> super.onOptionsItemSelected(item)
	}

	override fun onDetach() {
		super.onDetach()
		callback = null
	}

	fun loadRecyclerData() {
		disposables += singleGetCrimes
				.doOnSuccess { text_crimes_is_empty.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE }
				.doOnSuccess { crimeAdapter.updateAllCrimes(it) }
				.subscribe()
	}


	private class CrimeAdapter(
			private var crimes: MutableList<Crime> = mutableListOf<Crime>(),
			callback: Callback? = null
	) : RecyclerView.Adapter<CrimeHolder>() {

		private val refCallback: WeakReference<Callback?> = WeakReference(callback)

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CrimeHolder(parent, refCallback.get())

		override fun getItemCount() = crimes.size

		override fun onBindViewHolder(holder: CrimeHolder, position: Int) = holder.bind(crimes[position])

		fun updateAllCrimes(newCrimes: List<Crime>) {
			DiffUtil.calculateDiff(CrimeDiffCallback(crimes, newCrimes))
					.dispatchUpdatesTo(this)
			crimes = newCrimes.toMutableList()
		}

		fun removeCrime(position: Int): Crime {
			val crime = crimes.removeAt(position)
			notifyItemRemoved(position)
			return crime
		}
	}

	private class CrimeDiffCallback(
			private val oldCrimes: List<Crime>,
			private val newCrimes: List<Crime>)
		: DiffUtil.Callback() {

		override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
				oldCrimes[oldItemPosition].id == newCrimes[newItemPosition].id

		override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
				oldCrimes[oldItemPosition] == newCrimes[newItemPosition]

		override fun getOldListSize() = oldCrimes.size

		override fun getNewListSize() = newCrimes.size

	}

	interface Callback {
		fun onCrimeSelected(crimeId: Long)
	}

	private class CrimeHolder(parent: ViewGroup,
	                          callback: Callback? = null) :
			RecyclerView.ViewHolder(LayoutInflater.from(parent.context)
					.inflate(R.layout.list_item_crime, parent, false)),
			View.OnClickListener {

		private val title: TextView = itemView.findViewById(R.id.crime_title)
		private val date: TextView = itemView.findViewById(R.id.crime_date)
		private val crimeSolved: ImageView = itemView.findViewById(R.id.crime_solved)
		private val refCallback: WeakReference<Callback?> = WeakReference(callback)

		private lateinit var crime: Crime

		init {
			itemView.setOnClickListener(this)
		}

		fun bind(crime: Crime) {
			title.text = crime.title
			date.text = DateFormat.format("EEE, MMM dd, yyyy", crime.date)
			crimeSolved.visibility = if (crime.requiresPolicy) View.VISIBLE else View.GONE
			this.crime = crime
		}

		override fun onClick(p0: View) {
			refCallback.get()?.onCrimeSelected(crime.id)
		}
	}
}