package com.example.nail.criminalintent

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.ShareCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.text.format.DateFormat
import android.view.*
import android.widget.Toast
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import kotlinx.android.synthetic.main.fragment_crime.*
import java.io.File
import java.util.*

class CrimeFragment : Fragment() {

	private lateinit var singleGetCrime: Single<Crime>
	private val disposables = CompositeDisposable()
	private var crime: Crime? = null
	private var photoFile: File? = null

	private var callback: CrimeFragment.Callback? = null

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		if (context is CrimeFragment.Callback) {
			callback = context
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		val id = arguments?.get(ARG_CRIME_ID) as? Long
				?: throw IllegalArgumentException("arguments is null")
		setHasOptionsMenu(true)

		singleGetCrime = App.crimeDatabase.crimeDao().getOne(id)
				.subscribeOn(io())
				.observeOn(mainThread())

	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_crime, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		disposables += singleGetCrime
				.doOnSuccess { crime = it }
				.doOnSuccess {
					crime_title.setText(it.title)
					crime_solved.isChecked = it.solved
					val date = Date(it.date)
					updateCrimeSuspect(it.suspect)
					updateDateLabel(date)
					updateTimeLabel(date)
					photoFile = File(activity!!.filesDir, it.photoName).apply {
						updatePhotoView(this)
					}
				}
				.subscribe()

		with(crime_title) {
			afterTextChanged {
				val title = it
				crime?.let {
					it.title = title
					updateCrimeDb(it)
				}
			}
		}

		with(crime_solved) {
			setOnCheckedChangeListener { _, checked ->
				crime?.let {
					it.solved = checked
					updateCrimeDb(it)
				}
			}
		}

		with(crime_date) {
			setOnClickListener {
				crime?.let {
					DatePickerFragment.newInstance(Date(it.date)).apply {
						setTargetFragment(this@CrimeFragment, REQUEST_DATE)
					}.show(fragmentManager, DIALOG_DATE)
				}
			}
		}

		with(crime_time) {
			setOnClickListener {
				crime?.let {
					TimePickerFragment.newInstance(Date(it.date)).apply {
						setTargetFragment(this@CrimeFragment, REQUEST_TIME)
					}.show(fragmentManager, DIALOG_TIME)
				}
			}
		}

		with(crime_report) {
			setOnClickListener {
				crime?.let {
					val sendCrime = ShareCompat.IntentBuilder.from(activity).apply {
						setText(createCrimeReport(it))
						setSubject(getString(R.string.crime_report_subject))
						setType("text/plain")
					}.intent.let {
						Intent.createChooser(it, getString(R.string.crime_send_report))
					}
					startActivity(sendCrime)
				}
			}
		}

		with(crime_call) {
			val pickPhone = Intent().apply {
				action = Intent.ACTION_PICK
				data = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
			}
			isEnabled = activity!!.packageManager.resolveActivity(pickPhone, PackageManager.MATCH_DEFAULT_ONLY) != null
			setOnClickListener {
				callSuspect(pickPhone)
			}
		}

		with(crime_suspect) {
			afterTextChanged {
				val suspect = it
				crime?.let {
					it.suspect = suspect
					updateCrimeDb(it)
				}
			}
		}

		with(crime_suspect_contact) {
			val pickContact = Intent().apply {
				action = Intent.ACTION_PICK
				data = ContactsContract.Contacts.CONTENT_URI
			}
			isEnabled = activity!!.packageManager.resolveActivity(pickContact, PackageManager.MATCH_DEFAULT_ONLY) != null
			setOnClickListener { startActivityForResult(pickContact, REQUEST_CONTACT) }
		}

		with(crime_photo) {
			val captureImage = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
				action = MediaStore.ACTION_IMAGE_CAPTURE
			}

			isEnabled = captureImage.resolveActivity(this@CrimeFragment.activity!!.packageManager) != null

			setOnLongClickListener {
				val photo = photoFile ?: return@setOnLongClickListener true
				val uriForFile = FileProvider.getUriForFile(this@CrimeFragment.activity!!,
						"com.example.nail.criminalintent.fileprovider", photo)

				captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile)

				activity!!.packageManager.queryIntentActivities(captureImage, PackageManager.MATCH_DEFAULT_ONLY).forEach {
					this@CrimeFragment.activity!!.grantUriPermission(it.activityInfo.packageName,
							uriForFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
				}
				startActivityForResult(captureImage, REQUEST_PHOTO)
				true
			}

			setOnClickListener {
				photoFile?.let {
					if (it.exists()) {
						PhotoFragment.newInstance(it.path).show(fragmentManager, DIALOG_PHOTO)
					} else {
						performLongClick()
					}
				}
			}
		}

		reqPermissions()
	}

	override fun onPause() {
		disposables.clear()
		super.onPause()
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater)
		inflater.inflate(R.menu.crime_fragment, menu)
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
		R.id.delete_crime -> {
			crime?.let {
				disposables += Single.fromCallable { App.crimeDatabase.crimeDao().delete(it) }
						.subscribeOn(io())
						.observeOn(mainThread())
						.doOnSuccess {
							callback?.onCrimeUpdated()
							if (resources.getBoolean(R.bool.is_phone)) {
								activity!!.finish()
							} else {
								fragmentManager!!.beginTransaction()
										.remove(this)
										.commit()
							}
						}
						.subscribe()
			}
			true
		}
		else -> super.onOptionsItemSelected(item)
	}

	override fun onDetach() {
		super.onDetach()
		callback = null
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (resultCode != Activity.RESULT_OK) return

		when (requestCode) {
			REQUEST_DATE -> {
				data?.let {
					val date = DatePickerFragment.readDate(data)
					crime?.let {
						it.date = date.time
						updateCrimeDb(it)
						updateDateLabel(date)
					}
				}
			}
			REQUEST_TIME -> {
				data?.let {
					val date = TimePickerFragment.readDate(data)
					crime?.let {
						it.date = date.time
						updateCrimeDb(it)
						updateTimeLabel(date)
					}
				}
			}
			REQUEST_CONTACT -> {
				data?.let {
					val uri = data.data
					val cursor = activity!!.contentResolver
							.query(uri, arrayOf(ContactsContract.Contacts.DISPLAY_NAME), null, null, null)
					cursor.use {
						getDisplayName(cursor)?.let {
							val suspect = it
							crime?.let {
								it.suspect = suspect
								updateCrimeDb(it)
								updateCrimeSuspect(suspect)
							}
						}
					}
				}
			}
			REQUEST_PHOTO -> {
				val photo = photoFile ?: return
				val uriForFile = FileProvider.getUriForFile(this@CrimeFragment.activity!!,
						"com.example.nail.criminalintent.fileprovider", photo)

				activity!!.revokeUriPermission(uriForFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

				updatePhotoView(photo)
			}
			else -> super.onActivityResult(requestCode, resultCode, data)

		}
	}

	private fun callSuspect(pickPhone: Intent) {
		val uri = pickPhone.data
		val cursor = activity!!.contentResolver
				.query(uri, arrayOf(
						ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
						ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER,
						ContactsContract.CommonDataKinds.Phone.NUMBER),
						null, null, null)
		cursor.use {
			findPhoneByDisplayName(it, crime?.suspect)?.let {
				val intent = Intent().apply {
					action = Intent.ACTION_DIAL
					this.data = Uri.parse("tel:$it")
				}
				startActivity(intent)
			}
		}
	}

	private fun reqPermissions() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (ContextCompat.checkSelfPermission(this@CrimeFragment.activity!!, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(this@CrimeFragment.activity!!, arrayOf(Manifest.permission.READ_CONTACTS), REQUEST_PERMISSION_READ_CONTACTS)
			}
		}
	}

	private fun updateDateLabel(date: Date) {
		crime_date.text = DateFormat.format("MMM dd yyyy", date)
	}

	private fun updateTimeLabel(date: Date) {
		crime_time.text = DateFormat.format("HH:mm", date)
	}

	private fun updateCrimeSuspect(suspect: String) {
		crime_suspect.setText(suspect)
	}

	private fun updateCrimeDb(crime: Crime) {
		disposables += (Single.fromCallable { App.crimeDatabase.crimeDao().update(crime) }
				.subscribeOn(io())
				.observeOn(mainThread())
				.doOnSuccess { callback?.onCrimeUpdated() }
				.subscribe())
	}

	private fun updatePhotoView(photo: File) {
		if (photo.exists()) {
			disposables += Observable.just(photo)
					.subscribeOn(Schedulers.io())
					.map { it.getScaledBitmap(it.path, activity!!) }
					.observeOn(mainThread())
					.doOnNext { crime_photo.setImageBitmap(it) }
					.subscribe()
		}
	}

	private fun createCrimeReport(crime: Crime): String = getString(R.string.crime_report,

			crime.title,

			DateFormat.format("EEE, MMM dd", Date(crime.date)),

			if (crime.solved) getString(R.string.crime_report_solved) else getString(R.string.crime_report_unsolved),

			if (crime.suspect.isNotBlank()) getString(R.string.crime_report_suspect, crime.suspect) else getString(R.string.crime_report_no_suspect)
	)

	private fun getDisplayName(cursor: Cursor): String? {
		with(cursor) {
			if (count > 0) {
				moveToFirst()
				return getString(0)
			}
			return null
		}
	}

	private fun findPhoneByDisplayName(cursor: Cursor, name: String?): String? {
		cursor.forEach {
			val displayName = it.getString(0)
			if (displayName == name) {
				return if (it.getInt(1) == 1) {
					it.getString(2)
				} else {
					Toast.makeText(this@CrimeFragment.context, "Contact hasn't phone :(", Toast.LENGTH_SHORT).show()
					null
				}
			}
		}
		return null
	}

	private inline fun Cursor.forEach(action: (Cursor) -> Unit) {
		moveToFirst()
		while (!isAfterLast) {
			action(this)
			moveToNext()
		}
	}

	interface Callback {
		fun onCrimeUpdated()
	}

	companion object {
		private const val ARG_CRIME_ID = "crime_id"
		private const val DIALOG_DATE = "DialogDate"
		private const val DIALOG_TIME = "DialogTime"
		private const val DIALOG_PHOTO = "DialogPhoto"
		private const val REQUEST_DATE = 103
		private const val REQUEST_TIME = 104
		private const val REQUEST_CONTACT = 105
		private const val REQUEST_PHOTO = 106
		private const val REQUEST_PERMISSION_READ_CONTACTS = 107

		fun newFragment(crimeId: Long): CrimeFragment {
			val bundle = Bundle()
			bundle.putSerializable(ARG_CRIME_ID, crimeId)

			val fragment = CrimeFragment()
			fragment.arguments = bundle
			return fragment
		}
	}
}
